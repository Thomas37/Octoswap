﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamInertie : MonoBehaviour {

    public float accelX;
    public float accelY;
    public float inertieParTick;
    public float inertieMax;
    public float inertiex;
    public float inertiey;
    private int signex;
    // Use this for initialization
    void Start()
    {
        signex = 1;
    }

    // Update is called once per frame
    void Update()
    {

        inertieProcess();

        var x = inertiex * Time.deltaTime * accelX * -1;
        var z = inertiey * Time.deltaTime * accelY * -1;
        //transform.Rotate(0, x, 0);
        transform.Translate(x, 0, z);
    }

    void inertieProcess()
    {
        if (signex==1 && inertiex > inertieMax - inertieParTick)
        {
            signex = -1;
        }
        if(signex==-1 && Input.GetAxis("Horizontal") == 0)
        {
            signex = 1;
        }

        if (Input.GetAxis("Horizontal") > 0 && ((inertiex < inertieMax - inertieParTick && signex == 1) || (signex==-1 && inertiex > inertieParTick)))
        {
            inertiex += inertieParTick*signex;
        }
        else if (Input.GetAxis("Horizontal") < 0 && ((inertiex > inertieParTick - inertieMax && signex == 1) || (signex == -1 && inertiex < -inertieParTick)))
        {
            inertiex -= inertieParTick*signex;
        }
        else
        {
            if (inertiex > 0)
            {
                inertiex -= inertieParTick;
            }
            else if (inertiex < 0)
            {
                inertiex += inertieParTick;
            }
        }

        if (Input.GetAxis("Vertical") > 0 && inertiey < inertieMax - inertieParTick)
        {
            inertiey += inertieParTick;
        }
        else if (Input.GetAxis("Vertical") < 0 && inertiey > inertieParTick - inertieMax)
        {
            inertiey -= inertieParTick;
        }
        else
        {
            if (inertiey > 0)
            {
                inertiey -= inertieParTick;
            }
            else if (inertiey < 0)
            {
                inertiey += inertieParTick;
            }
        }

        if (Mathf.Abs(inertiex) < inertieParTick)
        {
            inertiex = 0;
        }
        if (Mathf.Abs(inertiey) < inertieParTick)
        {
            inertiey = 0;
        }
    }
}
