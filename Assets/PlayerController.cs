﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {
    private float speedX;
    private float speedY;
    public float speed;
    public float speedMax;

    // Use this for initialization
    void Start () {
        speedX = 0;
        speedY = 0;
    }
	
	// Update is called once per frame
	void Update () {
        bool xPositif = speedX > 0;
        bool xNul = speedX == 0;
        bool yPositif = speedY > 0;
        bool yNul = speedY == 0;

        if (Input.GetAxis("Horizontal") == 0 || (Input.GetAxis("Horizontal") < 0 && speedX > 0) || (Input.GetAxis("Horizontal") > 0 && speedX < 0))
        {
            if(speedX > 0)
            {
                speedX -= Time.deltaTime;
            }
            else if(speedX < 0)
            {
                speedX += Time.deltaTime;
            }
        }
        else
        {
            speedX += Time.deltaTime * Input.GetAxis("Horizontal");
        }
        if(speedX > speedMax)
        {
            speedX = speedMax;
        }
        else if(speedX < -speedMax)
        {
            speedX = -speedMax;
        }

        if (Input.GetAxis("Vertical") == 0)
        {
            if (speedY > 0)
            {
                speedY -= Time.deltaTime;
            }
            else if (speedY < 0)
            {
                speedY += Time.deltaTime;
            }
        }
        else
        {
            speedY += Time.deltaTime * Input.GetAxis("Vertical");
        }
        if (speedY > speedMax)
        {
            speedY = speedMax;
        }
        else if (speedY < -speedMax)
        {
            speedY = -speedMax;
        }

        if(xPositif != (speedX > 0) && !xNul)
        {
            speedX = 0;
        }
        if (yPositif != (speedY > 0) && !yNul)
        {
            speedY = 0;
        }

        var x = Math.Pow(speedX * speed,3)+ speedX * speed * 0.5;
        var z = Math.Pow(speedY * speed,3)+ speedY * speed * 0.5;
        //transform.Rotate(0, x, 0);
        transform.Translate((float)x, 0, (float)z);
        transform.position = new Vector3(transform.position.x,2f, transform.position.z);
    }
}
